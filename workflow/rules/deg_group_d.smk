rule DEG_Group_D:
    input:
        file = 'result/Mapping/Merged_seurat.rds'
    params:
        threads = 4
    output:
        groupD =report("result/DEG/DF_Group_D.txt", caption="report/DEG/DF_Group_D.txt", category="DEG",subcategory="Group_D")
    shell:
        "Rscript workflow/scripts/2.3.DEG_Group_D.R {input.file} {params.threads} {output.groupD}"
